# Apache Hadoop Framework HDFS MapReduce Programming Model

MapReduce programs for: VideoCount task (similar to WordCount example https://hadoop.apache.org/docs/stable/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html#Example:_WordCount_v1.0), matrix addition, matrix multiplication.

Implemented in Java.

My lab assignment in Networked Systems Middleware, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: Nov 2021

