import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class MatrixElement implements Writable {
    private IntWritable matrixOrder;
    private Text matrixName;
    private IntWritable rowColIndex;
    private DoubleWritable matrixElementValue;

    public MatrixElement() {
        matrixOrder = new IntWritable(0);
        matrixName = new Text("");
        rowColIndex = new IntWritable(0);
        matrixElementValue = new DoubleWritable(0.0);
    }

    public MatrixElement(IntWritable matrixOrder, Text matrixName, IntWritable rowColIndex, DoubleWritable matrixElementValue) {
        this.matrixOrder = matrixOrder;
        this.matrixName = matrixName;
        this.rowColIndex = rowColIndex;
        this.matrixElementValue = matrixElementValue;
    }

    public void write(DataOutput dataOutput) throws IOException {
        matrixOrder.write(dataOutput);
        matrixName.write(dataOutput);
        rowColIndex.write(dataOutput);
        matrixElementValue.write(dataOutput);
    }

    public void readFields(DataInput dataInput) throws IOException {
        matrixOrder.readFields(dataInput);
        matrixName.readFields(dataInput);
        rowColIndex.readFields(dataInput);
        matrixElementValue.readFields(dataInput);
    }

    public IntWritable getMatrixOrder() {
        return matrixOrder;
    }

    public void setMatrixOrder(IntWritable matrixOrder) {
        this.matrixOrder = matrixOrder;
    }

    public Text getMatrixName() {
        return matrixName;
    }

    public void setMatrixName(Text matrixName) {
        this.matrixName = matrixName;
    }

    public IntWritable getRowColIndex() {
        return rowColIndex;
    }

    public void setRowColIndex(IntWritable rowColIndex) {
        this.rowColIndex = rowColIndex;
    }

    public DoubleWritable getMatrixElementValue() {
        return matrixElementValue;
    }

    public void setMatrixElementValue(DoubleWritable matrixElementValue) {
        this.matrixElementValue = matrixElementValue;
    }

    @Override
    public String toString() {
        return matrixOrder + " " + matrixName + " " + rowColIndex + " " + matrixElementValue;
    }
}
