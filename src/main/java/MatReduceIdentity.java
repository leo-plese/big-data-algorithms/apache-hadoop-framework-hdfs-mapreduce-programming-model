import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;


public class MatReduceIdentity extends Reducer<IntWritable, MatrixElement, IntWritable, MatrixElement> {

    @Override
    public void reduce(IntWritable key, Iterable<MatrixElement> values, Context context)
            throws IOException, InterruptedException {

        for (MatrixElement value : values) {
            context.write(key, value);
        }
    }
}
