import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class MatMap1 extends Mapper<LongWritable, Text, IntWritable, MatrixElement> {

    private Map<String, Integer> matrixNameToOrderMap = new HashMap<String, Integer>();
    private boolean firstRecordEntered = false;

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String[] parts = (value.toString()).split(" ");
        String matrixName = parts[0];
        int rowIndex = Integer.parseInt(parts[1]);
        int colIndex = Integer.parseInt(parts[2]);
        double matrixElementValue = Double.parseDouble(parts[3]);

        if (matrixNameToOrderMap.containsKey(matrixName)) {
            int matrixNumber = matrixNameToOrderMap.get(matrixName);
            if (matrixNumber == 0) {
                System.out.println("LEO: mat 1 first false : (" + matrixNumber + ", " + matrixName + ", " + rowIndex + ", " + colIndex + ", " + matrixElementValue + ")");
                context.write(new IntWritable(colIndex), new MatrixElement(new IntWritable(matrixNumber), new Text(matrixName), new IntWritable(rowIndex), new DoubleWritable(matrixElementValue)));
            } else {
                System.out.println("LEO: mat 2 first false : (" + matrixNumber + ", " + matrixName + ", " + rowIndex + ", " + colIndex + ", " + matrixElementValue + ")");
                context.write(new IntWritable(rowIndex), new MatrixElement(new IntWritable(matrixNumber), new Text(matrixName), new IntWritable(colIndex), new DoubleWritable(matrixElementValue)));
            }
        } else {
            if (!firstRecordEntered) {
                System.out.println("LEO: mat 1 first true : (" + 0 + ", " + matrixName + ", " + rowIndex + ", " + colIndex + ", " + matrixElementValue + ")");
                context.write(new IntWritable(colIndex), new MatrixElement(new IntWritable(0), new Text(matrixName), new IntWritable(rowIndex), new DoubleWritable(matrixElementValue)));

                matrixNameToOrderMap.put(matrixName, 0);
                firstRecordEntered = true;
            } else {
                System.out.println("LEO: mat 2 first true : (" + 1 + ", " + matrixName + ", " + rowIndex + ", " + colIndex + ", " + matrixElementValue + ")");
                context.write(new IntWritable(rowIndex), new MatrixElement(new IntWritable(1), new Text(matrixName), new IntWritable(colIndex), new DoubleWritable(matrixElementValue)));

                matrixNameToOrderMap.put(matrixName, 1);
            }
        }
    }
}
