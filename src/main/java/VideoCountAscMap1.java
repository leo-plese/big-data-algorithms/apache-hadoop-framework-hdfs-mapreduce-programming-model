import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class VideoCountAscMap1 extends Mapper<LongWritable, Text, Text, IntWritable> {

    private static final int THRESHOLD = 5;

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String[] parts = (value.toString()).split(" ");
        String id = parts[0];
        int percent = Integer.parseInt(parts[1]);

        if (percent > THRESHOLD) {
            context.write(new Text(id), new IntWritable(percent));
        }
    }
}
