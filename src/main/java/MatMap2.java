import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;

// IDENTITY
public class MatMap2 extends Mapper<LongWritable, Text, MatrixIndicesPair, DoubleWritable> {

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        System.out.println("LEO: entered map2...");
        String[] parts = (value.toString()).split("\\s+");
        System.out.println("LEO: before parsing...");
        int rowIndex = Integer.parseInt(parts[0]);
        int colIndex = Integer.parseInt(parts[1]);
        double matrixElementValue = Double.parseDouble(parts[2]);

        System.out.println("LEO: after parsing: "+rowIndex+" "+colIndex+" "+matrixElementValue);

        context.write(new MatrixIndicesPair(new IntWritable(rowIndex), new IntWritable(colIndex)), new DoubleWritable(matrixElementValue));
    }
}
