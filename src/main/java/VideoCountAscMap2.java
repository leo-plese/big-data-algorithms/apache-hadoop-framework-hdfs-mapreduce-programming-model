import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

// IDENTITY function
public class VideoCountAscMap2 extends Mapper<LongWritable, Text, IntWritable, Text> {

    @Override
    public void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {

        String[] parts = (value.toString()).split("\\s+");
        String id = parts[1];
        int percent = Integer.parseInt(parts[0]);



        context.write(new IntWritable(percent), new Text(id));

    }
}
