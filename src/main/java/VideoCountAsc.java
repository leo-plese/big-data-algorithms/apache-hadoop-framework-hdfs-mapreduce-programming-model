import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class VideoCountAsc {

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: VideoCount <input path> <output path>");
            System.exit(-1);
        }

        Job job1 = Job.getInstance();
        job1.setJobName("Video count");
        job1.setJarByClass(VideoCountAsc.class);

        FileInputFormat.addInputPath(job1, new Path(args[0]));
        FileOutputFormat.setOutputPath(job1, new Path(args[1]+"/intermed"));

        job1.setMapperClass(VideoCountAscMap1.class);
        job1.setReducerClass(VideoCountAscReduce1.class);

        job1.setMapOutputKeyClass(Text.class);
        job1.setMapOutputValueClass(IntWritable.class);
        job1.setOutputKeyClass(IntWritable.class);
        job1.setOutputValueClass(Text.class);

        job1.setOutputFormatClass(TextOutputFormat.class);

        job1.waitForCompletion(true);

        Job job2 = Job.getInstance();
        job2.setJobName("Video count asc sort");
        job2.setJarByClass(VideoCountAsc.class);

        FileInputFormat.addInputPath(job2, new Path(args[1]+"/intermed"));
        FileOutputFormat.setOutputPath(job2, new Path(args[1]+"/final"));

        job2.setMapperClass(VideoCountAscMap2.class);
        job2.setReducerClass(VideoCountAscReduce2.class);

        job2.setMapOutputKeyClass(IntWritable.class);
        job2.setMapOutputValueClass(Text.class);
        job2.setOutputKeyClass(Text.class);
        job2.setOutputValueClass(IntWritable.class);

        job2.setInputFormatClass(TextInputFormat.class);
        job2.setOutputFormatClass(TextOutputFormat.class);

        job2.waitForCompletion(true);

        ///////////////////


    }

}