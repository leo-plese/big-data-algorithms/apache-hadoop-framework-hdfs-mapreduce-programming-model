import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MatReduce1 extends Reducer<IntWritable, MatrixElement, MatrixIndicesPair, DoubleWritable> {

    @Override
    public void reduce(IntWritable key, Iterable<MatrixElement> values, Context context)
            throws IOException, InterruptedException {

        int count = 0;
        List<MatrixElement> matrixElementsList = new ArrayList<MatrixElement>();


        Configuration configuration = context.getConfiguration();
        for (MatrixElement value : values) {
            matrixElementsList.add(WritableUtils.clone(value, configuration));
            count += 1;
        }

        System.out.println("LEO: list (len = " + count + ") for key = " + key + " : [");
        for (MatrixElement element : matrixElementsList) {
            System.out.println(element);
        }
        System.out.println("]");

        for (int i=0; i<count-1; i++) {
            MatrixElement mat1Element = matrixElementsList.get(i);
            String mat1Name = mat1Element.getMatrixName().toString();
            double mat1ElementValue = mat1Element.getMatrixElementValue().get();
            int mat1Order = mat1Element.getMatrixOrder().get();

            for (int j=i+1; j<count; j++) {
                MatrixElement mat2Element = matrixElementsList.get(j);
                String mat2Name = mat2Element.getMatrixName().toString();

                if (mat1Name.equals(mat2Name)) {
                    continue;
                }

                double mat2ElementValue = mat2Element.getMatrixElementValue().get();

                if (mat1Order == 0) {
                    System.out.println("LEO: reduce key = " + key + " mat 1 of ORDER 0 :::: " + "(mat " + mat1Name +  " , order " + mat1Order + " , " + "index " + mat1Element.getRowColIndex() + " , value " + mat1ElementValue + ") - " +  "(mat " + mat2Name +  " , order " + mat2Element.getMatrixOrder() + " , " + "index " + mat2Element.getRowColIndex() + " , value " + mat2ElementValue + ")");
                    context.write(new MatrixIndicesPair(mat1Element.getRowColIndex(), mat2Element.getRowColIndex()), new DoubleWritable(mat1ElementValue * mat2ElementValue));
                } else {
                    System.out.println("LEO: reduce key = " + key + " mat 1 of ORDER 1 :::: " + "(mat " + mat1Name +  " , order " + mat1Order + " , " + "index " + mat1Element.getRowColIndex() + " , value " + mat1ElementValue + ") - " +  "(mat " + mat2Name +  " , order " + mat2Element.getMatrixOrder() + " , " + "index " + mat2Element.getRowColIndex() + " , value " + mat2ElementValue + ")");
                    context.write(new MatrixIndicesPair(mat2Element.getRowColIndex(), mat1Element.getRowColIndex()), new DoubleWritable(mat1ElementValue * mat2ElementValue));
                }
            }

        }

    }
}
