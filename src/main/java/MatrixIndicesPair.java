import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class MatrixIndicesPair implements WritableComparable<MatrixIndicesPair> {
    private IntWritable rowIndex;
    private IntWritable colIndex;

    public MatrixIndicesPair() {
        rowIndex = new IntWritable(0);
        colIndex = new IntWritable(0);
    }

    public MatrixIndicesPair(IntWritable rowIndex, IntWritable colIndex) {
        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
    }

    public void write(DataOutput dataOutput) throws IOException {
        rowIndex.write(dataOutput);
        colIndex.write(dataOutput);
    }

    public void readFields(DataInput dataInput) throws IOException {
        rowIndex.readFields(dataInput);
        colIndex.readFields(dataInput);
    }

    public IntWritable getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(IntWritable rowIndex) {
        this.rowIndex = rowIndex;
    }

    public IntWritable getColIndex() {
        return colIndex;
    }

    public void setColIndex(IntWritable colIndex) {
        this.colIndex = colIndex;
    }

    @Override
    public String toString() {
        return rowIndex + " " + colIndex;
    }


    public int compareTo(MatrixIndicesPair otherMatrixIndicesPair) {
        int thisRowIndex = rowIndex.get(), otherRowIndex = otherMatrixIndicesPair.rowIndex.get();

        if (thisRowIndex < otherRowIndex) {
            return -1;
        } else if (thisRowIndex > otherRowIndex) {
            return 1;
        } else {    // thisRowIndex == otherRowIndex
            int thisColIndex = colIndex.get(), otherColIndex = otherMatrixIndicesPair.colIndex.get();
            if (thisColIndex < otherColIndex) {
                return -1;
            } else if (thisColIndex > otherColIndex) {
                return 1;
            } else {    // thisColIndex == otherColIndex
                return 0;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatrixIndicesPair that = (MatrixIndicesPair) o;
        return (rowIndex.get() == that.rowIndex.get()) &&
                (colIndex.get() == that.colIndex.get());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + rowIndex.get();
        result = prime * result + (int) (colIndex.get() ^ (colIndex.get() >>> 16));
        return result;
    }
}
