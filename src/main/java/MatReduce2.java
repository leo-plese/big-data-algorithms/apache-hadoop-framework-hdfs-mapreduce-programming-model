import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;


public class MatReduce2 extends Reducer<MatrixIndicesPair, DoubleWritable, MatrixIndicesPair, DoubleWritable> {

    @Override
    public void reduce(MatrixIndicesPair key, Iterable<DoubleWritable> values, Context context)
            throws IOException, InterruptedException {

        double matrixResultElement = 0;
        for (DoubleWritable value : values) {
            matrixResultElement += value.get();
        }

        context.write(key, new DoubleWritable(matrixResultElement));
    }
}
