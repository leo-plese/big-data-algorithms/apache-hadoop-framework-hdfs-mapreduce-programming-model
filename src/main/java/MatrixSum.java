import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;


public class MatrixSum {

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: MatrixSum <input path> <output path>");
            System.exit(-1);
        }


        Job job1 = Job.getInstance();
        job1.setJobName("Matrix sum");
        job1.setJarByClass(MatrixSum.class);

        FileInputFormat.addInputPath(job1, new Path(args[0]));
        FileOutputFormat.setOutputPath(job1, new Path(args[1]));

        job1.setMapperClass(MatSumMap.class);
        job1.setReducerClass(MatSumReduce.class);

        job1.setMapOutputKeyClass(MatrixIndicesPair.class);
        job1.setMapOutputValueClass(DoubleWritable.class);
        job1.setOutputKeyClass(MatrixIndicesPair.class);
        job1.setOutputValueClass(DoubleWritable.class);

        job1.setOutputFormatClass(TextOutputFormat.class);

        job1.waitForCompletion(true);

        ///////////////////


    }

}