import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;


public class MatSumMap extends Mapper<LongWritable, Text, MatrixIndicesPair, DoubleWritable> {

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String[] parts = (value.toString()).split(" ");
        int rowIndex = Integer.parseInt(parts[1]);
        int colIndex = Integer.parseInt(parts[2]);
        double matrixElementValue = Double.parseDouble(parts[3]);

        context.write(new MatrixIndicesPair(new IntWritable(rowIndex), new IntWritable(colIndex)), new DoubleWritable(matrixElementValue));
    }
}
