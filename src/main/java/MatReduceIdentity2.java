import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


public class MatReduceIdentity2 extends Reducer<MatrixIndicesPair, DoubleWritable, MatrixIndicesPair, DoubleWritable> {

    @Override
    public void reduce(MatrixIndicesPair key, Iterable<DoubleWritable> values, Context context)
            throws IOException, InterruptedException {

        for (DoubleWritable value : values) {
            context.write(key, value);
        }
    }
}
