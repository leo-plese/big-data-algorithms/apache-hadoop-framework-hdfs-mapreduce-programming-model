import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;


public class MatrixMultiplication {

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: MatrixMultiplication <input path> <output path>");
            System.exit(-1);
        }


        Job job1 = Job.getInstance();
        job1.setJobName("Matrix multiplication part 1");
        job1.setJarByClass(MatrixMultiplication.class);

        FileInputFormat.addInputPath(job1, new Path(args[0]));
        FileOutputFormat.setOutputPath(job1, new Path(args[1]+"/intermed"));

        job1.setMapperClass(MatMap1.class);
        job1.setReducerClass(MatReduce1.class);
//        job1.setReducerClass(MatReduceIdentity.class);

        job1.setMapOutputKeyClass(IntWritable.class);
        job1.setMapOutputValueClass(MatrixElement.class);
        job1.setOutputKeyClass(MatrixIndicesPair.class);
        job1.setOutputValueClass(DoubleWritable.class);
//        job1.setOutputKeyClass(IntWritable.class);
//        job1.setOutputValueClass(MatrixElement.class);

        job1.setOutputFormatClass(TextOutputFormat.class);

        job1.waitForCompletion(true);


        Job job2 = Job.getInstance();
        job2.setJobName("Matrix multiplication part 2");
        job2.setJarByClass(MatrixMultiplication.class);

        FileInputFormat.addInputPath(job2, new Path(args[1]+"/intermed"));
        FileOutputFormat.setOutputPath(job2, new Path(args[1]+"/final"));

        job2.setMapperClass(MatMap2.class);
        job2.setReducerClass(MatReduce2.class);
//        job2.setReducerClass(MatReduceIdentity2.class);


        job2.setMapOutputKeyClass(MatrixIndicesPair.class);
        job2.setMapOutputValueClass(DoubleWritable.class);
        job2.setOutputKeyClass(MatrixIndicesPair.class);
        job2.setOutputValueClass(DoubleWritable.class);
//        job2.setOutputKeyClass(MatrixIndicesPair.class);
//        job2.setOutputValueClass(DoubleWritable.class);


        job2.setInputFormatClass(TextInputFormat.class);
        job2.setOutputFormatClass(TextOutputFormat.class);

        job2.waitForCompletion(true);

        ///////////////////


    }

}